import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class S {
  S(this.locale);

  final Locale locale;

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'uk': {
      'appName': 'Тестове завдання',
      'connectionError': 'Перевірте з\'єднання інтернетом',
      'repeatBtn': 'Повторити',
      'loadModeBtn': 'Показати ще',
    },
    'en': {
      'appName': 'Test Task',
      'connectionError': 'Check your Internet connection',
      'repeatBtn': 'Repeat',
      'loadModeBtn': 'Show more',
    },
    'ru': {
      'appName': 'Тестовое задание',
      'connectionError': 'Проверьте соединение с интернетом',
      'repeatBtn': 'Повторить',
      'loadModeBtn': 'Показать еще',
    },
  };

  String _get(String name) =>
      _localizedValues[locale.languageCode][name] ?? name;

  String get appName => _get('appName');
  String get connectionError => _get('connectionError');
  String get repeatBtn => _get('repeatBtn');
  String get loadModeBtn => _get('loadModeBtn');
}

class DemoLocalizationsDelegate extends LocalizationsDelegate<S> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      ['uk', 'en', 'ru'].contains(locale.languageCode);

  @override
  Future<S> load(Locale locale) {
    return SynchronousFuture<S>(S(locale));
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}
