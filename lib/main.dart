import 'package:flutter/material.dart';
import 'package:test_task/ui/list/list.dart';

import 'resources/strings.dart';
import 'ui/image/image.dart';

void main() {
  runApp(MyApp());
}

var routes = <String, PageBuilder>{
  ListPage.routeName: (BuildContext context, Object arg) => ListPage(),
  ImagePage.routeName: (BuildContext context, Object arg) =>
      ImagePage(arg: arg),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: ListPage.routeName,
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) {
            return routes[settings.name].call(context, settings.arguments);
          },
        );
      },
      localizationsDelegates: [DemoLocalizationsDelegate()],
      onGenerateTitle: (BuildContext context) => S.of(context).appName,
      supportedLocales: [
        const Locale('en', ''),
        const Locale('ru', ''),
        const Locale('uk', ''),
      ],
    );
  }
}

typedef PageBuilder = Widget Function(BuildContext, Object);
