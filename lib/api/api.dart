import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:test_task/app_config.dart';
import 'response/photo_response.dart';
import 'response/response.dart';

class Api {
  Future<ResponseBase<PhotosResponse>> photos(int page) {
    return _get<PhotosResponse>(PhotosResponse(), 'photos',
        queries: {'page': page.toString()});
  }

  Future<ResponseBase<T>> _get<T extends JsonBase>(T base, String url,
      {Map<String, String> queries}) async {
    try {
      var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Client-ID $apiToken'
      };
      String queryStr = '';
      queries?.forEach((k, v) {
        queryStr += (queryStr.length > 0 ? '&' : '') + '$k=$v';
      });
      url = apiUrl + url + (queryStr.length > 0 ? '?$queryStr' : '');
      var response =
          await http.get(url, headers: headers).timeout(Duration(seconds: 5));
      if (response.statusCode == 200) {
        try {
          var data = json.decode(response.body);
          return ResponseBase<T>(
              statusCode: response.statusCode,
              data: base.fromJson(data),
              headers: Headers(response.headers));
        } catch (ex) {
          return ResponseBase<T>(internalError: InternalErrorType.ServerError);
        }
      } else {
        return ResponseBase<T>(statusCode: response.statusCode);
      }
    } catch (ex) {
      print(ex);
      return ResponseBase<T>(
          internalError: InternalErrorType.NetworkConnection);
    }
  }
}
