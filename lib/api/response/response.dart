

class ResponseBase<T> {
  int statusCode;
  T data;
  Headers headers = Headers(Map());
  InternalErrorType internalError;

  bool isSuccess() => statusCode == 200 && internalError == null;

  bool isConnectionError() => internalError != null && internalError == InternalErrorType.NetworkConnection;

  ResponseBase({this.statusCode, this.data, this.internalError, this.headers});
}

abstract class JsonBase {
  JsonBase fromJson(dynamic json);
  Map toJson() => Map();
}

enum InternalErrorType {
  ServerError,
  NetworkConnection,
}

class Headers {
  Map<String, String> data;

  Headers(this.data);
}

jsonToList<T extends JsonBase>(List json, T map(dynamic e)) {
  if (json == null) return null;
  if (json.length > 0) {
    return json.map<T>(map).toList();
  } else {
    return List<T>();
  }
}

jsonToObject<T extends JsonBase>(dynamic json, JsonBase object) {
  if (json == null) return null;
  object.fromJson(json);
  return object;
}