import 'response.dart';

class PhotosResponse extends JsonBase {
  List<PhotoResponse> photos;

  @override
  PhotosResponse fromJson(dynamic json) {
    photos = jsonToList(json, (i) => PhotoResponse().fromJson(i));
    return this;
  }
}

class PhotoResponse extends JsonBase {
  String id, description, altDescription, color;
  PhotoUrlsResponse urls;
  PhotoUserResponse user;

  @override
  PhotoResponse fromJson(dynamic json) {
    id = json['id'];
    description = json['description'];
    altDescription = json['alt_description'];
    color = json['color'];
    urls = jsonToObject(json['urls'], PhotoUrlsResponse());
    user = jsonToObject(json['user'], PhotoUserResponse());
    return this;
  }
}

class PhotoUrlsResponse extends JsonBase {
  String raw, full, regular, small, thumb;

  @override
  PhotoUrlsResponse fromJson(dynamic json) {
    raw = json['raw'];
    full = json['full'];
    regular = json['regular'];
    small = json['small'];
    thumb = json['thumb'];
    return this;
  }
}

class PhotoUserResponse extends JsonBase {
  String name;

  @override
  PhotoUserResponse fromJson(dynamic json) {
    name = json['name'];
    return this;
  }
}
