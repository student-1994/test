import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task/api/response/photo_response.dart';
import 'package:test_task/ui/image/image.dart';
import 'package:test_task/ui/list/bloc/list_bloc.dart';
import 'package:test_task/resources/strings.dart';

class ListPage extends StatelessWidget {
  static final routeName = '/';

  const ListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ListBloc>(
      create: (context) => ListBloc(),
      child: _ListWidget(),
    );
  }
}

class _ListWidget extends StatelessWidget {
  const _ListWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ListBloc, ListState>(
        bloc: BlocProvider.of<ListBloc>(context)..add(ListLoad()),
        builder: (context, state) {
          if (state is ListError)
            return Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Text(S.of(context).connectionError),
                ),
                RaisedButton(
                  onPressed: () => _onRepeatClick(context),
                  child: Text(S.of(context).repeatBtn),
                )
              ],
            ));

          if (state is ListData)
            return CustomScrollView(
              slivers: <Widget>[
                SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                        (context, index) => ImageItem(
                              model: state.items[index],
                              index: index,
                              onTap: () => _onPhotoClick(
                                  context, index, state.items[index]),
                            ),
                        childCount: state.items.length),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, childAspectRatio: 1.0)),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Center(
                      child: () {
                        if (state.loading) return CircularProgressIndicator();
                        if (state.error)
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(S.of(context).connectionError),
                              RaisedButton(
                                onPressed: () => _onRepeatClick(context),
                                child: Text(S.of(context).repeatBtn),
                              )
                            ],
                          );
                        else
                          return RaisedButton(
                            onPressed: () => _onRepeatClick(context),
                            child: Text(S.of(context).loadModeBtn),
                          );
                      }(),
                    ),
                  ),
                ),
              ],
            );

          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  void _onRepeatClick(BuildContext context) {
    BlocProvider.of<ListBloc>(context).add(ListLoad());
  }

  void _onPhotoClick(BuildContext context, int index, PhotoResponse model) {
    Navigator.of(context).pushNamed(ImagePage.routeName,
        arguments: ImageArg(model: model, index: index));
  }
}

class ImageItem extends StatelessWidget {
  final PhotoResponse model;
  final int index;
  final GestureTapCallback onTap;
  const ImageItem({Key key, this.model, this.onTap, this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Positioned.fill(
              child: Hero(
                  tag: 'image' + index.toString(),
                  child: Image.network(model.urls.small, fit: BoxFit.cover))),
          Positioned.directional(
            textDirection: TextDirection.ltr,
            bottom: 0,
            start: 0,
            end: 0,
            child: Container(
              padding:
                  const EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 16),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Colors.transparent,
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5)
                  ])),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    model.user.name,
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                  Text(
                    model.description ?? model.altDescription ?? 'none',
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                        fontSize: 12, color: Colors.white.withOpacity(0.6)),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
