part of 'list_bloc.dart';

@immutable
abstract class ListEvent {}

class ListLoad extends ListEvent {}
