part of 'list_bloc.dart';

@immutable
abstract class ListState {}

class ListLoading extends ListState {}

class ListError extends ListState {}

class ListData extends ListState {
  final List<PhotoResponse> items;
  final bool error, loading;

  ListData({this.items, this.error = false, this.loading = false});
}
