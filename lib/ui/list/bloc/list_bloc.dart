import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_task/api/api.dart';
import 'package:test_task/api/response/photo_response.dart';

part 'list_event.dart';
part 'list_state.dart';

class ListBloc extends Bloc<ListEvent, ListState> {
  final List<PhotoResponse> items = [];
  int page = 1;

  @override
  ListState get initialState => ListLoading();

  @override
  Stream<ListState> mapEventToState(
    ListEvent event,
  ) async* {
    if (event is ListLoad) yield* _laod(event);
  }

  Stream<ListState> _laod(ListLoad event) async* {
    if (state is ListData)
      yield ListData(items: items, loading: true);
    else
      yield ListLoading();

    final response = await Api().photos(page);
    if (response.isSuccess()) {
      page++;
      items.addAll(response.data.photos);
      yield ListData(items: items);
    } else {
      if (state is ListData)
        yield ListData(items: items, error: true);
      else
        yield ListError();
    }
  }
}
