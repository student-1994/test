import 'package:flutter/material.dart';
import 'package:test_task/api/response/photo_response.dart';

class ImagePage extends StatefulWidget {
  static final routeName = '/list';
  final ImageArg arg;

  ImagePage({Key key, this.arg}) : super(key: key);

  @override
  _ImagePageState createState() => _ImagePageState();
}

class ImageArg {
  final PhotoResponse model;
  final int index;

  ImageArg({this.model, this.index});
}

class _ImagePageState extends State<ImagePage> with TickerProviderStateMixin {
  bool showUI = true;
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween<double>(begin: 1, end: 0).animate(_controller);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.arg.model.color != null
          ? _hexToColor(widget.arg.model.color)
          : Colors.black,
      body: Stack(
        children: <Widget>[
          Positioned.fill(
              child: GestureDetector(
            onTap: () => changeUIVisible(),
            child: Hero(
                tag: 'image' + widget.arg.index.toString(),
                child: Image.network(widget.arg.model.urls.small,
                    fit: BoxFit.contain)),
          )),
          Positioned.directional(
            textDirection: TextDirection.ltr,
            bottom: 0,
            start: 0,
            end: 0,
            child: AnimatedBuilder(
              animation: _controller,
              builder: (context, child) => Opacity(
                opacity: _animation.value,
                child: Container(
                  padding: const EdgeInsets.only(
                      left: 8, right: 8, bottom: 8, top: 16),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Colors.transparent,
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5)
                      ])),
                  child: SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.arg.model.user.name,
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                        Text(
                          widget.arg.model.description ??
                              widget.arg.model.altDescription ??
                              'none',
                          maxLines: 5,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              fontSize: 14, color: Colors.white.withOpacity(0.6)),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned.directional(
              textDirection: TextDirection.ltr,
              start: 0,
              top: 0,
              child: SafeArea(
                child: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () => _onBackClick(context)),
              ))
        ],
      ),
    );
  }

  void changeUIVisible() {
    showUI = !showUI;
    if (showUI) {
      _animation = Tween<double>(begin: 0, end: 1).animate(_controller);
    } else {
      _animation = Tween<double>(begin: 1, end: 0).animate(_controller);
    }
    _controller.forward(from: 0);
  }

  void _onBackClick(BuildContext context) {
    Navigator.of(context).pop();
  }

  Color _hexToColor(String code) {
    return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}
